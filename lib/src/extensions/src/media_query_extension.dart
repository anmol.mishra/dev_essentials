part of '../extensions.dart';

extension MediaQueryAndThemeExtension on DevEssential {
  BuildContext get context {
    final BuildContext mainContext = key.currentContext!;
    BuildContext? devicePreviewContext = of(mainContext).devicePreviewContext;
    return devicePreviewContext ?? mainContext;
  }

  BuildContext? get overlayContext {
    BuildContext? overlay;
    key.currentState?.overlay?.context.visitChildElements((element) {
      overlay = element;
    });
    return overlay;
  }

  MediaQueryData get mediaQuery => context.mediaQuery;

  Size get size => mediaQuery.size;

  double get width => context.width;

  double get height => context.height;

  double heightTransformer({double dividedBy = 1, double reducedBy = 0.0}) =>
      context.heightTransformer(
        dividedBy: dividedBy,
        reducedBy: reducedBy,
      );

  double widthTransformer({double dividedBy = 1, double reducedBy = 0.0}) =>
      context.widthTransformer(
        dividedBy: dividedBy,
        reducedBy: reducedBy,
      );

  double ratio({
    double dividedBy = 1,
    double reducedByW = 0.0,
    double reducedByH = 0.0,
  }) =>
      context.ratio(
        dividedBy: dividedBy,
        reducedByW: reducedByW,
        reducedByH: reducedByH,
      );

  double get statusBarHeight => mediaQuery.padding.top;

  double get bottomBarHeight => mediaQuery.padding.bottom;

  EdgeInsets get viewInsets => context.viewInsets;

  EdgeInsets get viewPadding => context.viewPadding;

  double get textScaleFactor => context.textScaleFactor;

  FocusNode? get focusScope => FocusManager.instance.primaryFocus;

  Orientation get orientation => context.orientation;

  bool get isLandscape => context.isLandscape;

  bool get isPortrait => context.isPortrait;

  double get mediaQueryShortestSide => context.mediaQueryShortestSide;

  bool get isPhoneOrLess => context.isPhoneOrLess;

  bool get isPhoneOrWider => context.isPhoneOrWider;

  bool get isPhone => context.isPhone;

  bool get isSmallTabletOrLess => context.isSmallTabletOrLess;

  bool get isSmallTabletOrWider => context.isSmallTabletOrWider;

  bool get isSmallTablet => context.isSmallTablet;

  bool get isLargeTablet => context.isLargeTablet;

  bool get isLargeTabletOrLess => context.isLargeTabletOrLess;

  bool get isLargeTabletOrWider => context.isLargeTabletOrWider;

  bool get isTablet => context.isTablet;

  bool get isDesktopOrLess => context.isDesktopOrLess;

  bool get isDesktopOrWider => context.isDesktopOrWider;

  bool get isDesktop => context.isDesktop;

  T responsiveValue<T>({
    T? watch,
    T? mobile,
    T? tablet,
    T? desktop,
  }) =>
      context.responsiveValue(
        watch: watch,
        mobile: mobile,
        tablet: tablet,
        desktop: desktop,
      );

  void setTheme(ThemeData themeData) => context.setTheme(themeData);

  void setThemeMode(ThemeMode themeMode) => context.setThemeMode(themeMode);

  ThemeData get theme => context.theme;

  TextTheme get textTheme => context.textTheme;

  bool get isDarkMode => context.isDarkMode;

  Color? get iconColor => context.iconColor;

  FlutterView get view => context.view;

  Locale? get deviceLocale => context.deviceLocale;

  double get pixelRatio => context.pixelRatio;

  Size get physicalDeviceSize => context.physicalDeviceSize;

  double get physicalDevicewidth => context.physicalDevicewidth;

  double get physicalDeviceheight => context.physicalDeviceheight;

  double get physicalDeviceStatusBarHeight =>
      context.physicalDeviceStatusBarHeight;

  double get physicalDeviceBottomBarHeight =>
      context.physicalDeviceBottomBarHeight;

  ViewPadding get physicalDeviceViewInsets => context.physicalDeviceViewInsets;

  ViewPadding get physicalDeviceViewPadding =>
      context.physicalDeviceViewPadding;

  double get physicalDeviceTextScaleFactor =>
      context.physicalDeviceTextScaleFactor;

  bool get isPlatformDarkMode => context.isPlatformDarkMode;
}
