part of '../extensions.dart';

extension ContextExtension on BuildContext {
  static final DevEssentialHookState _hookState =
      DevEssentialHookState.instance;

  MediaQueryData get mediaQuery => MediaQuery.of(this);

  Size get mediaQuerySize => mediaQuery.size;

  double get height => mediaQuerySize.height;

  double get width => mediaQuerySize.width;

  double heightTransformer({double dividedBy = 1, double reducedBy = 0.0}) {
    return (mediaQuerySize.height -
            ((mediaQuerySize.height / 100) * reducedBy)) /
        dividedBy;
  }

  double widthTransformer({double dividedBy = 1, double reducedBy = 0.0}) {
    return (mediaQuerySize.width - ((mediaQuerySize.width / 100) * reducedBy)) /
        dividedBy;
  }

  double ratio({
    double dividedBy = 1,
    double reducedByW = 0.0,
    double reducedByH = 0.0,
  }) =>
      heightTransformer(dividedBy: dividedBy, reducedBy: reducedByH) /
      widthTransformer(dividedBy: dividedBy, reducedBy: reducedByW);

  ThemeData get theme => _hookState.theme;

  void setTheme(ThemeData themeData) => _hookState.changeThemeData(themeData);

  void setThemeMode(ThemeMode themeMode) =>
      _hookState.changeThemeMode(themeMode);

  bool get isDarkMode => (theme.brightness == Brightness.dark);

  Color? get iconColor => theme.iconTheme.color;

  TextTheme get textTheme => theme.textTheme;

  EdgeInsets get padding => mediaQuery.padding;

  EdgeInsets get viewPadding => mediaQuery.viewPadding;

  EdgeInsets get viewInsets => mediaQuery.viewInsets;

  Orientation get orientation => mediaQuery.orientation;

  bool get isLandscape => orientation == Orientation.landscape;

  bool get isPortrait => orientation == Orientation.portrait;

  double get textScaleFactor => mediaQuery.textScaleFactor;

  double get mediaQueryShortestSide => mediaQuerySize.shortestSide;

  bool get isPhoneOrLess => width <= 600;

  bool get isPhoneOrWider => width >= 600;

  bool get isPhone => (mediaQueryShortestSide < 600);

  bool get isSmallTabletOrLess => width <= 600;

  bool get isSmallTabletOrWider => width >= 600;

  bool get isSmallTablet => (mediaQueryShortestSide >= 600);

  bool get isLargeTablet => (mediaQueryShortestSide >= 720);

  bool get isLargeTabletOrLess => width <= 720;

  bool get isLargeTabletOrWider => width >= 720;

  bool get isTablet => isSmallTablet || isLargeTablet;

  bool get isDesktopOrLess => width <= 1200;

  bool get isDesktopOrWider => width >= 1200;

  bool get isDesktop => isDesktopOrLess;

  T responsiveValue<T>({
    T? watch,
    T? mobile,
    T? tablet,
    T? desktop,
  }) {
    assert(
        watch != null || mobile != null || tablet != null || desktop != null);

    var deviceWidth = mediaQuerySize.width;
    //big screen width can display smaller sizes
    final strictValues = [
      if (deviceWidth >= 1200) desktop, //desktop is allowed
      if (deviceWidth >= 600) tablet, //tablet is allowed
      if (deviceWidth >= 300) mobile, //mobile is allowed
      watch, //watch is allowed
    ].whereType<T>();
    final looseValues = [
      watch,
      mobile,
      tablet,
      desktop,
    ].whereType<T>();
    return strictValues.firstOrNull ?? looseValues.first;
  }

  FlutterView get view => View.of(this);

  Locale? get deviceLocale => view.platformDispatcher.locale;

  double get pixelRatio => view.devicePixelRatio;

  Size get physicalDeviceSize => view.physicalSize / pixelRatio;

  double get physicalDevicewidth => physicalDeviceSize.width;

  double get physicalDeviceheight => physicalDeviceSize.height;

  double get physicalDeviceStatusBarHeight => view.padding.top;

  double get physicalDeviceBottomBarHeight => view.padding.bottom;

  ViewPadding get physicalDeviceViewInsets => view.viewInsets;

  ViewPadding get physicalDeviceViewPadding => view.viewPadding;

  double get physicalDeviceTextScaleFactor =>
      view.platformDispatcher.textScaleFactor;

  bool get isPlatformDarkMode =>
      (view.platformDispatcher.platformBrightness == Brightness.dark);
}
