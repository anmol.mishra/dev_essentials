part of '../../widgets.dart';

class ChooseChipItem<T> {
  const ChooseChipItem({
    required this.value,
    required this.child,
    this.width = 100,
    this.height,
  });

  final T value;
  final WidgetBuilder child;
  final double width;
  final double? height;
}

typedef ErrorWidgetBuilder = Widget Function(
    BuildContext context, String? errorText);

class DevEssentialReactiveChooseChip<T> extends ReactiveFormField<T, T> {
  DevEssentialReactiveChooseChip({
    Key? key,
    String? formControlName,
    FormControl<T>? formControl,
    Map<String, ValidationMessageFunction>? validationMessages,
    bool Function(AbstractControl<dynamic>)? showErrorsCallback,
    InputDecoration decoration = const InputDecoration(),
    required List<ChooseChipItem<T>> items,
    bool readOnly = false,
    Widget? disabledHint,
    WidgetBuilder? selectedItemBuilder,
    Color? fillColor,
    bool filled = false,
    EdgeInsetsGeometry? contentPadding,
    String? hintText,
    bool showHintText = true,
    TextStyle? hintStyle,
    String? labelText,
    TextStyle? labelStyle,
    TextStyle? errorStyle,
    bool enabelBorder = true,
    InputBorder? border,
    TextStyle? style,
    ReactiveFormFieldCallback<T>? onTap,
    ReactiveFormFieldCallback<T>? onChanged,
    Widget? icon,
    Color? iconDisabledColor,
    Color? iconEnabledColor,
    double iconSize = 24.0,
    bool canScroll = false,
    double height = kMinInteractiveDimension,
    ErrorWidgetBuilder? customErrorWidget,
  })  : _items = items,
        _readOnly = readOnly,
        _disabledHint = disabledHint,
        _selectedItemBuilder = selectedItemBuilder,
        super(
          key: key,
          formControlName: formControlName,
          formControl: formControl,
          validationMessages: validationMessages,
          showErrors: showErrorsCallback,
          builder: (ReactiveFormFieldState<T, T> field) {
            final _DevEssentialReactiveChooseChip<T> state =
                field as _DevEssentialReactiveChooseChip<T>;

            final InputDecoration effectiveDecoration = decoration
                .applyDefaults(Theme.of(state.context).inputDecorationTheme)
                .copyWith(
                  errorText: state.errorText,
                  error:
                      customErrorWidget?.call(state.context, state.errorText),
                  enabled: !state.isDisabled,
                  fillColor: fillColor,
                  filled: filled,
                  contentPadding: contentPadding,
                  hintText: hintText ??
                      (showHintText ? formControlName?.capitalize : null),
                  hintStyle: hintStyle ??
                      Theme.of(state.context).textTheme.bodyMedium!.copyWith(
                            fontSize: 16,
                          ),
                  labelText: labelText,
                  labelStyle: labelStyle ??
                      Theme.of(state.context).textTheme.bodyMedium!.copyWith(
                            fontSize: 16,
                          ),
                  errorStyle: errorStyle ??
                      Theme.of(state.context).inputDecorationTheme.errorStyle,
                  focusedBorder: !enabelBorder ? InputBorder.none : border,
                  errorBorder: !enabelBorder
                      ? InputBorder.none
                      : border?.copyWith(
                          borderSide: border.borderSide.copyWith(
                            color: Theme.of(state.context).colorScheme.error,
                          ),
                        ),
                  border: !enabelBorder ? InputBorder.none : border,
                  enabledBorder: !enabelBorder ? InputBorder.none : border,
                  disabledBorder: !enabelBorder ? InputBorder.none : border,
                  focusedErrorBorder: !enabelBorder ? InputBorder.none : border,
                  alignLabelWithHint: true,
                );

            late Widget childWidget;

            if (canScroll) {
              childWidget = SizedBox(
                height: height,
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  separatorBuilder: (_, __) => const SizedBox(
                    width: 16.0,
                  ),
                  itemBuilder: (context, index) {
                    final ChooseChipItem<T> item = items[index];
                    return SizedBox(
                      width: item.width,
                      height: item.height,
                      child: InkWell(
                        onTap: state.isDisabled
                            ? null
                            : () {
                                state.didChange(item.value);
                                onChanged?.call(state.control);
                              },
                        child: items[index].child.call(context),
                      ),
                    );
                  },
                  itemCount: items.length,
                ),
              );
            } else {
              childWidget = Wrap(
                runSpacing: 16.0,
                spacing: 16.0,
                children: items
                    .map(
                      (e) => SizedBox(
                        width: e.width,
                        height: e.height ?? height,
                        child: InkWell(
                          onTap: state.isDisabled
                              ? null
                              : () {
                                  state.didChange(e.value);
                                  onChanged?.call(state.control);
                                },
                          child: e.child.call(state.context),
                        ),
                      ),
                    )
                    .toList(),
              );
            }
            return FormField<T>(
              enabled: !state.isDisabled,
              builder: (_) => InputDecorator(
                baseStyle: style,
                isFocused: state.focusNode.hasFocus,
                isEmpty: state.effectiveValue == null,
                decoration: effectiveDecoration,
                child: childWidget,
              ),
            );
          },
        );

  final bool _readOnly;
  final Widget? _disabledHint;
  final List<ChooseChipItem<T>> _items;
  final WidgetBuilder? _selectedItemBuilder;

  @override
  ReactiveFormFieldState<T, T> createState() =>
      _DevEssentialReactiveChooseChip<T>();
}

class _DevEssentialReactiveChooseChip<T>
    extends ReactiveFocusableFormFieldState<T, T> {
  late T? effectiveValue;
  late bool isDisabled;
  Widget? effectiveDisabledHint;

  @override
  void initState() {
    super.initState();
    _initDropdownData();
  }

  void _initDropdownData() {
    final DevEssentialReactiveChooseChip<T> currentWidget =
        widget as DevEssentialReactiveChooseChip<T>;

    effectiveValue = value;
    if (effectiveValue != null &&
        !currentWidget._items.any((item) => item.value == effectiveValue)) {
      effectiveValue = null;
    }

    isDisabled = currentWidget._readOnly || control.disabled;
    effectiveDisabledHint = currentWidget._disabledHint;
    if (isDisabled && currentWidget._disabledHint == null) {
      final int selectedItemIndex = currentWidget._items
          .indexWhere((item) => item.value == effectiveValue);
      if (selectedItemIndex > -1) {
        effectiveDisabledHint = currentWidget._selectedItemBuilder != null
            ? currentWidget._selectedItemBuilder!(context)
            : currentWidget._items
                .elementAt(selectedItemIndex)
                .child
                .call(context);
      }
    }
  }

  @override
  void didChange(T? value) {
    effectiveValue = value;
    super.didChange(value);
  }

  @override
  void onControlValueChanged(dynamic value) {
    effectiveValue = value as T?;
    super.onControlValueChanged(value);
  }
}
