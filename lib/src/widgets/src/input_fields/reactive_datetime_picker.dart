part of '../../widgets.dart';

class DevEssentialReactiveDateTimePickerField<T>
    extends ReactiveFormField<T, DateTime> {
  DevEssentialReactiveDateTimePickerField({
    Key? key,
    String? formControlName,
    FormControl<T>? formControl,
    Map<String, ValidationMessageFunction>? validationMessages,
    bool Function(AbstractControl<dynamic>)? showErrorsCallback,
    InputDecoration decoration = const InputDecoration(),
    bool readOnly = false,
    Widget? disabledHint,
    WidgetBuilder? selectedItemBuilder,
    Color? fillColor,
    bool filled = false,
    EdgeInsetsGeometry? contentPadding,
    bool showHintText = true,
    TextStyle? hintStyle,
    String? labelText,
    TextStyle? labelStyle,
    TextStyle? errorStyle,
    bool enabelBorder = true,
    InputBorder? border,
    TextStyle? style,
    ReactiveFormFieldCallback<DateTime>? onTap,
    ReactiveFormFieldCallback<DateTime>? onChanged,
    Widget? suffixIcon,
    Widget? prefixIcon,
    Color? iconDisabledColor,
    Color? iconEnabledColor,
    double iconSize = 24.0,
    bool canScroll = false,
    double height = kMinInteractiveDimension,
    ErrorWidgetBuilder? customErrorWidget,
    String hintText = "DD/MM/YYYY",
    DateTime? pickerStartDate,
    DateTime? pickerEndDate,
  })  : _readOnly = readOnly,
        _disabledHint = disabledHint,
        super(
          key: key,
          formControlName: formControlName,
          validationMessages: validationMessages,
          showErrors: showErrorsCallback,
          builder: (ReactiveFormFieldState<T, DateTime> field) {
            final _ReactiveDateTimePickerFieldState<DateTime> state =
                field as _ReactiveDateTimePickerFieldState<DateTime>;
            DateTime startDate = pickerStartDate ?? DateTime(1950);
            DateTime endDate = pickerEndDate ?? DateTime(3000);

            final InputDecoration effectiveDecoration = decoration
                .applyDefaults(Dev.theme.inputDecorationTheme)
                .copyWith(
                  suffixIcon: suffixIcon,
                  prefixIcon: prefixIcon,
                  errorText: state.errorText,
                  error:
                      customErrorWidget?.call(state.context, state.errorText),
                  enabled: !state.isDisabled,
                  fillColor: fillColor,
                  filled: filled,
                  contentPadding: contentPadding,
                  hintText: (showHintText ? hintText : null),
                  hintStyle: hintStyle ??
                      Theme.of(state.context).textTheme.bodyMedium!.copyWith(
                            fontSize: 16,
                          ),
                  labelText: labelText,
                  labelStyle: labelStyle ??
                      Theme.of(state.context).textTheme.bodyMedium!.copyWith(
                            fontSize: 16,
                          ),
                  errorStyle: errorStyle ??
                      Theme.of(state.context).inputDecorationTheme.errorStyle,
                  focusedBorder: !enabelBorder ? InputBorder.none : border,
                  errorBorder: !enabelBorder
                      ? InputBorder.none
                      : border?.copyWith(
                          borderSide: border.borderSide.copyWith(
                            color: Theme.of(state.context).colorScheme.error,
                          ),
                        ),
                  border: !enabelBorder ? InputBorder.none : border,
                  enabledBorder: !enabelBorder ? InputBorder.none : border,
                  disabledBorder: !enabelBorder ? InputBorder.none : border,
                  focusedErrorBorder: !enabelBorder ? InputBorder.none : border,
                  alignLabelWithHint: true,
                );

            return FormField<T>(
              enabled: !state.isDisabled,
              builder: (_) => InputDecorator(
                baseStyle: style,
                isFocused: state.focusNode.hasFocus,
                isEmpty: state.value == null,
                decoration: effectiveDecoration,
                child: InkWell(
                  onTap: () {
                    Dev.dialog(
                      Dialog(
                        child: ClipRRect(
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          child: SfDateRangePicker(
                            minDate: startDate,
                            maxDate: endDate,
                            showActionButtons: false,
                            showTodayButton: false,
                            view: DateRangePickerView.month,
                            viewSpacing: 5,
                            showNavigationArrow: true,
                            selectionTextStyle: Theme.of(state.context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: 14,
                                ),
                            backgroundColor:
                                Theme.of(state.context).scaffoldBackgroundColor,
                            onSelectionChanged: (
                              DateRangePickerSelectionChangedArgs args,
                            ) {
                              state.didChange(args.value);
                              onChanged?.call(state.control);
                              Navigator.pop(state.context);
                            },
                          ),
                        ),
                      ),
                    ).then(
                      (_) {
                        if (!state.control.isNotNull) {
                          state.control.markAsTouched();
                        }
                      },
                    );
                  },
                  child: SizedBox(
                    width: double.maxFinite,
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        state.value?.toddMMyyyy() ?? '',
                        textAlign: TextAlign.left,
                        textScaleFactor: Dev.textScaleFactor,
                      ),
                    ),
                  ),
                ),
              ),
            );
          },
        );

  final bool _readOnly;
  final Widget? _disabledHint;

  @override
  ReactiveFormFieldState<T, DateTime> createState() =>
      _ReactiveDateTimePickerFieldState<T>();
}

class _ReactiveDateTimePickerFieldState<T>
    extends ReactiveFormFieldState<T, DateTime> {
  late FocusController _focusController;
  FocusNode? _focusNode;
  late DateTime? effectiveValue;
  late bool isDisabled;
  Widget? effectiveDisabledHint;
  FocusNode get focusNode => _focusNode ?? _focusController.focusNode;

  @override
  void initState() {
    super.initState();
    _initPicker();
  }

  void _initPicker() {
    final DevEssentialReactiveDateTimePickerField<T> currentWidget =
        widget as DevEssentialReactiveDateTimePickerField<T>;

    effectiveValue = value;

    isDisabled = currentWidget._readOnly || control.disabled;
    effectiveDisabledHint = currentWidget._disabledHint;
  }

  @override
  void subscribeControl() {
    _registerFocusController(FocusController());
    super.subscribeControl();
  }

  @override
  void dispose() {
    _unregisterFocusController();
    super.dispose();
  }

  void _onChanged(DateTime value, ValueChanged<DateTime>? callBack) {
    didChange(value);
    callBack?.call(value);
  }

  void _registerFocusController(FocusController focusController) {
    _focusController = focusController;
    control.registerFocusController(focusController);
  }

  void _unregisterFocusController() {
    control.unregisterFocusController(_focusController);
    _focusController.dispose();
  }
}
